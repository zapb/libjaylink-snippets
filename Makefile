CC = gcc

SRCS = $(wildcard *.c)
TARGETS = $(SRCS:%.c=%)

all: $(TARGETS)

%: %.c
	$(CC) $< -Wall -g `pkg-config --cflags --libs libjaylink` -o $@

clean:
	rm -f $(TARGETS)
