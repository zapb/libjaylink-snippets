/*
 * Copyright (C) 2015-2016 Marc Schink <jaylink-dev@marcschink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>
#include <libjaylink/libjaylink.h>

#define EFM32_STK_CONFIG_CHANNEL	(JAYLINK_EMUCOM_CHANNEL_USER + 0x0)
#define PACKET_SIZE			15

static uint8_t packet[PACKET_SIZE] = {
	0x01, 0x00, 0x00, 0x00, 0x00, 0x03, 0x03, 0x05,
	0x00, 0x0c, 0x00, 0x02, 0x00, 0x00, 0x00
};

enum debug_mode {
	DEBUG_MODE_IN = 0x00,
	DEBUG_MODE_OUT = 0x01,
	DEBUG_MODE_MCU = 0x02,
	DEBUG_MODE_OFF = 0x03,
	DEBUG_MODE_ETM_IN = 0x04,
	DEBUG_MODE_ETM_OUT = 0x05,
	DEBUG_MODE_ETM_MCU = 0x06
};

int main(int argc, char **argv)
{
	ssize_t ret;
	struct jaylink_context *ctx;
	struct jaylink_device **devs;
	struct jaylink_device_handle *devh;
	uint8_t caps[JAYLINK_DEV_EXT_CAPS_SIZE];
	size_t i;
	bool use_serial_number;
	uint32_t serial_number;
	char *firmware_version;
	size_t length;
	bool device_found;
	uint32_t tmp;
	uint32_t size;

	if (argc < 2) {
		printf("Usage: %s <debug-mode> [serial number]\n", argv[0]);
		return EXIT_FAILURE;
	}

	if (!strcasecmp(argv[1], "in")) {
		packet[13] = DEBUG_MODE_IN;
	} else if (!strcasecmp(argv[1], "out")) {
		packet[13] = DEBUG_MODE_OUT;
	} else if (!strcasecmp(argv[1], "mcu")) {
		packet[13] = DEBUG_MODE_MCU;
	} else if (!strcasecmp(argv[1], "off")) {
		packet[13] = DEBUG_MODE_OFF;
	} else if (!strcasecmp(argv[1], "etm-in")) {
		packet[13] = DEBUG_MODE_ETM_IN;
	} else if (!strcasecmp(argv[1], "etm-out")) {
		packet[13] = DEBUG_MODE_ETM_OUT;
	} else if (!strcasecmp(argv[1], "etm-mcu")) {
		packet[13] = DEBUG_MODE_ETM_MCU;
	} else {
		printf("Invalid debug mode: %s.\n", argv[1]);
		printf("Valid debug modes are: 'in', 'out', 'mcu', 'off', "
			"'etm-in', 'etm-out' and 'etm-mcu'.\n");
		return EXIT_FAILURE;
	}

	use_serial_number = false;

	if (argc > 2) {
		ret = jaylink_parse_serial_number(argv[2], &serial_number);

		if (ret != JAYLINK_OK) {
			printf("Failed to parse serial number: %s.\n",
				argv[2]);
			return EXIT_FAILURE;
		}

		use_serial_number = true;
	}

	ret = jaylink_init(&ctx);

	if (ret != JAYLINK_OK) {
		printf("jaylink_init() failed: %s.\n",
			jaylink_strerror_name(ret));
		return EXIT_FAILURE;
	}

	ret = jaylink_discovery_scan(ctx, 0);

	if (ret != JAYLINK_OK) {
		printf("jaylink_discovery_scan() failed: %s.\n",
			jaylink_strerror_name(ret));
		jaylink_exit(ctx);
		return EXIT_FAILURE;
	}

	ret = jaylink_get_devices(ctx, &devs, NULL);

	if (ret != JAYLINK_OK) {
		printf("jaylink_get_device_list() failed: %s.\n",
			jaylink_strerror_name(ret));
		jaylink_exit(ctx);
		return EXIT_FAILURE;
	}

	device_found = false;

	for (i = 0; devs[i]; i++) {
		devh = NULL;
		ret = jaylink_device_get_serial_number(devs[i], &tmp);

		if (ret != JAYLINK_OK) {
			printf("jaylink_device_get_serial_number() failed: "
				"%s.\n", jaylink_strerror_name(ret));
			continue;
		}

		if (use_serial_number && serial_number != tmp)
			continue;

		ret = jaylink_open(devs[i], &devh);

		if (ret == JAYLINK_OK) {
			serial_number = tmp;
			device_found = true;
			break;
		}

		printf("jaylink_open() failed: %s.\n",
			jaylink_strerror_name(ret));
	}

	jaylink_free_devices(devs, true);

	if (!device_found) {
		printf("No J-Link device found.\n");
		jaylink_exit(ctx);
		return EXIT_SUCCESS;
	}

	printf("S/N: %012u\n", serial_number);

	ret = jaylink_get_firmware_version(devh, &firmware_version, &length);

	if (ret != JAYLINK_OK) {
                printf("jaylink_get_firmware_version() failed: %s.\n",
                        jaylink_strerror_name(ret));
                jaylink_close(devh);
                jaylink_exit(ctx);
                return EXIT_FAILURE;
	} else if (length > 0) {
		printf("Firmware: %s\n", firmware_version);
		free(firmware_version);
	}

	memset(caps, 0, JAYLINK_DEV_EXT_CAPS_SIZE);

	ret = jaylink_get_caps(devh, caps);

	if (ret != JAYLINK_OK) {
		printf("jaylink_get_caps() failed: %s.\n",
			jaylink_strerror_name(ret));
		jaylink_close(devh);
		jaylink_exit(ctx);
		return EXIT_FAILURE;
	}

	if (jaylink_has_cap(caps, JAYLINK_DEV_CAP_GET_EXT_CAPS)) {
		ret = jaylink_get_extended_caps(devh, caps);

		if (ret != JAYLINK_OK) {
			printf("jaylink_get_extended_caps() failed: %s.\n",
				jaylink_strerror_name(ret));
			jaylink_close(devh);
			jaylink_exit(ctx);
			return EXIT_FAILURE;
		}
	}

	if (!jaylink_has_cap(caps, JAYLINK_DEV_CAP_EMUCOM)) {
		printf("Device does not support EMUCOM.\n");
		jaylink_close(devh);
		jaylink_exit(ctx);
		return EXIT_SUCCESS;
	}

	size = PACKET_SIZE;
	ret = jaylink_emucom_write(devh, EFM32_STK_CONFIG_CHANNEL, packet,
		&size);

	if (ret == JAYLINK_ERR_DEV_NOT_SUPPORTED) {
		printf("Channel not supported by the device.\n");
		jaylink_close(devh);
		jaylink_exit(ctx);
		return EXIT_FAILURE;
	} else 	if (ret != JAYLINK_OK) {
		printf("jaylink_emucom_write() failed: %s.\n",
			jaylink_strerror_name(ret));
		jaylink_close(devh);
		jaylink_exit(ctx);
		return EXIT_FAILURE;
	}

	if (size != PACKET_SIZE)
		printf("Changing debug mode failed.\n");
	else
		printf("Debug mode changed successfully.\n");

	jaylink_close(devh);
	jaylink_exit(ctx);

	return EXIT_SUCCESS;
}
