/*
 * Copyright (C) 2015 Marc Schink <jaylink-dev@marcschink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <inttypes.h>
#include <libjaylink/libjaylink.h>

#define ARRAY_SIZE(x) (sizeof((x)) / sizeof((x)[0]))

static const struct {
	enum jaylink_target_interface iface;
	const char *name;
} target_ifaces[] = {
	{
		.iface = JAYLINK_TIF_JTAG,
		.name = "JTAG"
	},
	{
		.iface = JAYLINK_TIF_SWD,
		.name = "SWD"
	},
	{
		.iface = JAYLINK_TIF_BDM3,
		.name = "BDM3"
	},
	{
		.iface = JAYLINK_TIF_FINE,
		.name = "FINE"
	},
	{
		.iface = JAYLINK_TIF_2W_JTAG_PIC32,
		.name = "2-wire JTAG for PIC32"
	}
};

static void print_interfaces(struct jaylink_device_handle *devh,
		uint32_t interfaces)
{
	size_t i;

	printf("\nTarget interfaces: %02x\n", interfaces);

	for (i = 0; i < ARRAY_SIZE(target_ifaces); i++) {
		if (interfaces & (1 << target_ifaces[i].iface))
			printf(" - %s\n", target_ifaces[i].name);
	}
}

static bool print_interface_speeds(struct jaylink_device_handle *devh,
		uint32_t interfaces)
{
	int ret;
	size_t i;
	struct jaylink_speed speed;

	for (i = 0; i < ARRAY_SIZE(target_ifaces); i++) {
		if (!(interfaces & (1 << target_ifaces[i].iface)))
			continue;

		ret = jaylink_select_interface(devh, target_ifaces[i].iface,
			NULL);

		if (ret != JAYLINK_OK) {
			printf("jaylink_select_interface() failed: %s.\n",
				jaylink_strerror_name(ret));
			return false;
		}

		ret = jaylink_get_speeds(devh, &speed);

		if (ret != JAYLINK_OK) {
			printf("jaylink_get_speeds() failed: %s.\n",
				jaylink_strerror_name(ret));
			return false;
		}

		printf(" - %s: %" PRIu32 " MHz / div; div >= %u\n",
			target_ifaces[i].name, speed.freq / 1000000,
			speed.div);
	}

	return true;
}

int main(int argc, char **argv)
{
	ssize_t ret;
	struct jaylink_context *ctx;
	struct jaylink_device **devs;
	struct jaylink_device_handle *devh;
	uint8_t caps[JAYLINK_DEV_EXT_CAPS_SIZE];
	struct jaylink_hardware_version hwver;
	uint32_t interfaces;
	struct jaylink_swo_speed swo_speed;
	uint8_t config[JAYLINK_DEV_CONFIG_SIZE];
	bool use_serial_number;
	uint32_t serial_number;
	char *firmware_version;
	size_t length;
	bool device_found;
	uint32_t tmp;
	size_t i;

	if (argc > 2) {
		printf("Usage: %s [serial number]\n", argv[0]);
		return EXIT_FAILURE;
	}

	use_serial_number = false;

	if (argc > 1) {
		ret = jaylink_parse_serial_number(argv[1], &serial_number);

		if (ret != JAYLINK_OK) {
			printf("Failed to parse serial number: %s.\n",
				argv[1]);
			return EXIT_FAILURE;
		}

		use_serial_number = true;
	}

	ret = jaylink_init(&ctx);

	if (ret != JAYLINK_OK) {
		printf("jaylink_init() failed: %s.\n",
			jaylink_strerror_name(ret));
		return EXIT_FAILURE;
	}

	ret = jaylink_discovery_scan(ctx, 0);

	if (ret != JAYLINK_OK) {
		printf("jaylink_discovery_scan() failed: %s.\n",
			jaylink_strerror_name(ret));
		jaylink_exit(ctx);
		return EXIT_FAILURE;
	}

	ret = jaylink_get_devices(ctx, &devs, NULL);

	if (ret != JAYLINK_OK) {
		printf("jaylink_get_device_list() failed: %s.\n",
			jaylink_strerror_name(ret));
		jaylink_exit(ctx);
		return EXIT_FAILURE;
	}

	device_found = false;

	for (i = 0; devs[i]; i++) {
		devh = NULL;
		ret = jaylink_device_get_serial_number(devs[i], &tmp);

		if (ret != JAYLINK_OK) {
			printf("jaylink_device_get_serial_number() failed: "
				"%s.\n", jaylink_strerror_name(ret));
			continue;
		}

		if (use_serial_number && serial_number != tmp)
			continue;

		ret = jaylink_open(devs[i], &devh);

		if (ret == JAYLINK_OK) {
			serial_number = tmp;
			device_found = true;
			break;
		}

		printf("jaylink_open() failed: %s.\n",
			jaylink_strerror_name(ret));
	}

	jaylink_free_devices(devs, true);

	if (!device_found) {
		printf("No J-Link device found.\n");
		jaylink_exit(ctx);
		return EXIT_SUCCESS;
	}

	printf("S/N: %012u\n", serial_number);

	ret = jaylink_get_firmware_version(devh, &firmware_version, &length);

	if (ret != JAYLINK_OK) {
                printf("jaylink_get_firmware_version() failed: %s.\n",
                        jaylink_strerror_name(ret));
                jaylink_close(devh);
                jaylink_exit(ctx);
                return EXIT_FAILURE;
	} else if (length > 0) {
		printf("\nFirmware: %s\n", firmware_version);
		free(firmware_version);
	}

	memset(caps, 0, JAYLINK_DEV_EXT_CAPS_SIZE);

	ret = jaylink_get_caps(devh, caps);

	if (ret != JAYLINK_OK) {
		printf("jaylink_get_caps() failed: %s.\n",
			jaylink_strerror_name(ret));
		jaylink_close(devh);
		jaylink_exit(ctx);
		return EXIT_FAILURE;
	}

	if (jaylink_has_cap(caps, JAYLINK_DEV_CAP_GET_EXT_CAPS)) {
		ret = jaylink_get_extended_caps(devh, caps);

		if (ret != JAYLINK_OK) {
			printf("jaylink_get_extended_caps() failed: %s.\n",
				jaylink_strerror_name(ret));
			jaylink_close(devh);
			jaylink_exit(ctx);
			return EXIT_FAILURE;
		}
	}

	printf("\nDevice capabilities:\n");

	for (i = 0; i < JAYLINK_DEV_EXT_CAPS_SIZE; i++) {
		if (i > 0 && (i % 16) == 0)
			printf("\n");

		printf("%02x ", caps[i]);
	}

	printf("\n");

	if (jaylink_has_cap(caps, JAYLINK_DEV_CAP_GET_HW_VERSION)) {
		ret = jaylink_get_hardware_version(devh, &hwver);

		if (ret != JAYLINK_OK) {
			printf("jaylink_get_hardware_version() failed: %s.",
				jaylink_strerror_name(ret));
			jaylink_close(devh);
			jaylink_exit(ctx);
			return EXIT_FAILURE;
		}

		printf("\nHardware version: v%u.%02u (%u)\n", hwver.major,
			hwver.minor, hwver.type);
	}

	if (jaylink_has_cap(caps, JAYLINK_DEV_CAP_SELECT_TIF)) {
		ret = jaylink_get_available_interfaces(devh, &interfaces);

		if (ret != JAYLINK_OK) {
			printf("jaylink_get_available_interfaces() failed: "
				"%s.\n", jaylink_strerror_name(ret));
			return false;
		}
	} else {
		interfaces = (1 << JAYLINK_TIF_JTAG);
	}

	print_interfaces(devh, interfaces);

	if (jaylink_has_cap(caps, JAYLINK_DEV_CAP_GET_SPEEDS)) {
		printf("\nTarget interface speeds:\n");

		if (!print_interface_speeds(devh, interfaces)) {
			jaylink_close(devh);
			jaylink_exit(ctx);
			return EXIT_FAILURE;
		}

		if (jaylink_has_cap(caps, JAYLINK_DEV_CAP_ADAPTIVE_CLOCKING))
			printf(" - Adaptive clocking\n");
	}

	if (jaylink_has_cap(caps, JAYLINK_DEV_CAP_SWO)) {
		ret = jaylink_swo_get_speeds(devh, JAYLINK_SWO_MODE_UART,
			&swo_speed);

		if (ret != JAYLINK_OK) {
			printf("jaylink_swo_get_speeds() failed: "
				"%s.\n", jaylink_strerror_name(ret));
			jaylink_close(devh);
			jaylink_exit(ctx);
			return EXIT_FAILURE;
		}

		printf("\nSWO speeds: %u MHz / div; div >= %u and div <= %u\n",
			swo_speed.freq / 1000000, swo_speed.min_div,
			swo_speed.max_div);
	}

	if (jaylink_has_cap(caps, JAYLINK_DEV_CAP_READ_CONFIG)) {
		ret = jaylink_read_raw_config(devh, config);

		if (ret != JAYLINK_OK) {
			printf("jaylink_read_raw_config() failed: %s.\n",
				jaylink_strerror_name(ret));
			jaylink_close(devh);
			jaylink_exit(ctx);
			return EXIT_FAILURE;
		}

		printf("\nDevice configuration:\n");

		for (i = 0; i < JAYLINK_DEV_CONFIG_SIZE; i++) {
			if (i > 0 && (i % 16) == 0)
				printf("\n");

			printf("%02x ", config[i]);
		}

		printf("\n");
	}

	jaylink_close(devh);
	jaylink_exit(ctx);

	return EXIT_SUCCESS;
}
